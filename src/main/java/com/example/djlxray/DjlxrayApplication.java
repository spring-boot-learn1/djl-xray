package com.example.djlxray;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DjlxrayApplication {

	public static void main(String[] args) {
		SpringApplication.run(DjlxrayApplication.class, args);
	}

}
